<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table = 'users';

    protected $allowedFields = [
        'id_user_roles',
        'name',
        'email',
        'password',
        'image'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
