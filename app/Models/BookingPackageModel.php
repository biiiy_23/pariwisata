<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingPackageModel extends Model
{
    protected $table = 'booking_package';

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
