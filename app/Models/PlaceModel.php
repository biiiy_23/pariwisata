<?php

namespace App\Models;

use CodeIgniter\Model;

class PlaceModel extends Model
{
    protected $table = 'places';

    protected $allowedFields = [
        'place_name',
        'place_detail',
        'image'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
