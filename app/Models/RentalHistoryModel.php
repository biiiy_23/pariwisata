<?php

namespace App\Models;

use CodeIgniter\Model;

class RentalHistoryModel extends Model
{
    protected $table = 'booking_histories';

    protected $allowedFields = [
        'id_user',
        'id_booking_package',
        'booking_date',
        'vehicle',
        'id_tour_guide'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
