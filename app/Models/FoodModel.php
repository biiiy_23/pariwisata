<?php

namespace App\Models;

use CodeIgniter\Model;

class FoodModel extends Model
{
    protected $table = 'foods';

    protected $allowedFields = [
        'food_name',
        'food_detail',
        'image'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
