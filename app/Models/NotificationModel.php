<?php

namespace App\Models;

use CodeIgniter\Model;

class NotificationModel extends Model
{
    protected $table = 'notifications';

    protected $allowedFields = [
        'id_user',
        'notification_detail'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
