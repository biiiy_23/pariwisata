<?php

namespace App\Models;

use CodeIgniter\Model;

class CultureModel extends Model
{
    protected $table = 'cultures';

    protected $allowedFields = [
        'culture_name',
        'culture_detail',
        'image'
    ];

    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
}
