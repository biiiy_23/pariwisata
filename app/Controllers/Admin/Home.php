<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\NotificationModel;
use App\Models\RentalHistoryModel;
use App\Models\UsersModel;

class Home extends BaseController
{
    protected $notificationModel;
    protected $historyModel;
    protected $userModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->historyModel = new RentalHistoryModel();
        $this->userModel = new UsersModel();
    }

    public function index()
    {
        $now = date('Y-m-d');

        $data = [
            'title' => 'Dashboard',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'histories' => $this->historyModel
                ->findAll(),
            'currentHistory' => $this->historyModel
                ->select('booking_histories.*, booking_package.booking_package_name, tourist.name AS tourist_name, tour_guide.name AS tour_guide_name')
                ->join('users AS tourist', 'tourist.id=booking_histories.id_user')
                ->join('users AS tour_guide', 'tour_guide.id=booking_histories.id_tour_guide', 'LEFT')
                ->join('booking_package', 'booking_package.id=booking_histories.id_booking_package')
                ->like('booking_date', $now, 'after')
                ->findAll(),
            'tourist' => $this->userModel->where('id_user_roles', '2')
                ->findAll(),
            'tourGuides' => $this->userModel->where('id_user_roles', '3')
                ->findAll()
        ];

        // dd($data['histories']);

        // $time = strtotime($data['histories'][0]['booking_date']);
        // dd(date('Y-m-d', $time));

        return view('pages/admin/dashboard', $data);
    }
}
