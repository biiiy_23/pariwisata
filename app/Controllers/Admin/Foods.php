<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\FoodModel;
use App\Models\NotificationModel;

class Foods extends BaseController
{
    protected $notificationModel;
    protected $foodModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->foodModel = new FoodModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Foods',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'foods' => $this->foodModel->findAll()
        ];

        return view('pages/admin/foods/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Foods',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/foods/create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'image' => 'uploaded[image]|max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_add_foods')->withInput();
        }

        $image = $this->request->getFile('image');
        $image->move('assets/img/foods');
        $imageName = $image->getName();

        $this->foodModel->save([
            'food_name' => $this->request->getVar('foodName'),
            'food_detail' => $this->request->getVar('foodDetail'),
            'image' => $imageName
        ]);

        $this->session->setFlashdata('msg', 'Data has been added');
        return redirect()->route('foods_admin');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Foods',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'food' => $this->foodModel->where('id', $id)->first(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/foods/edit', $data);
    }

    public function update($id)
    {

        if (!$this->validate([
            'image' => 'max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_change_foods')->withInput();
        }

        $image = $this->request->getFile('image');

        if ($image->getName() == '') {
            $this->foodModel->save([
                'id' => $id,
                'food_name' => $this->request->getVar('foodName'),
                'food_detail' => $this->request->getVar('foodDetail')
            ]);
        } else {
            $image->move('assets/img/foods');
            $imageName = $image->getName();

            $this->foodModel->save([
                'id' => $id,
                'food_name' => $this->request->getVar('foodName'),
                'food_detail' => $this->request->getVar('foodDetail'),
                'image' => $imageName
            ]);
        }

        $this->session->setFlashdata('msg', 'Data has been edited');
        return redirect()->route('foods_admin');
    }

    public function destroy($id)
    {
        $this->foodModel->delete($id);
        $this->session->setFlashdata('msg', 'Data has been deleted');
        return redirect()->route('foods_admin');
    }
}
