<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\NotificationModel;

class Notifications extends BaseController
{
    protected $notificationModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Notifications',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll()
        ];

        return view('pages/admin/notifications', $data);
    }
}
