<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\CultureModel;
use App\Models\NotificationModel;

class Cultures extends BaseController
{
    protected $notificationModel;
    protected $cultureModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->cultureModel = new CultureModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Cultures',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'cultures' => $this->cultureModel->findAll()
        ];

        return view('pages/admin/cultures/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Cultures',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/cultures/create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'image' => 'uploaded[image]|max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_add_cultures')->withInput();
        }

        $image = $this->request->getFile('image');
        $image->move('assets/img/cultures');
        $imageName = $image->getName();

        $this->cultureModel->save([
            'culture_name' => $this->request->getVar('cultureName'),
            'culture_detail' => $this->request->getVar('cultureDetail'),
            'image' => $imageName
        ]);

        $this->session->setFlashdata('msg', 'Data has been added');
        return redirect()->route('cultures_admin');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Cultures',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'culture' => $this->cultureModel->where('id', $id)->first(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/cultures/edit', $data);
    }

    public function update($id)
    {

        if (!$this->validate([
            'image' => 'max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_change_cultures')->withInput();
        }

        $image = $this->request->getFile('image');

        if ($image->getName() == '') {
            $this->cultureModel->save([
                'id' => $id,
                'culture_name' => $this->request->getVar('cultureName'),
                'culture_detail' => $this->request->getVar('cultureDetail'),
            ]);
        } else {
            $image->move('assets/img/cultures');
            $imageName = $image->getName();

            $this->cultureModel->save([
                'id' => $id,
                'culture_name' => $this->request->getVar('cultureName'),
                'culture_detail' => $this->request->getVar('cultureDetail'),
                'image' => $imageName
            ]);
        }

        $this->session->setFlashdata('msg', 'Data has been edited');
        return redirect()->route('cultures_admin');
    }

    public function destroy($id)
    {
        $this->cultureModel->delete($id);
        $this->session->setFlashdata('msg', 'Data has been deleted');
        return redirect()->route('cultures_admin');
    }
}
