<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\NotificationModel;
use App\Models\UsersModel;

class Users extends BaseController
{
    protected $notificationModel;
    protected $userModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->userModel = new UsersModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Users',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'users' => $this->userModel->withDeleted()->paginate(5),
            'pager' => $this->userModel->pager
        ];

        return view('pages/admin/users/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Users',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/users/create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'image' => 'uploaded[image]|max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_add_users')->withInput();
        }

        $image = $this->request->getFile('image');
        $image->move('assets/img/users');
        $imageName = $image->getName();

        $this->userModel->save([
            'id_user_roles' => $this->request->getVar('role'),
            'name' => $this->request->getVar('name'),
            'email' => $this->request->getVar('email'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
            'image' => $imageName
        ]);

        $this->session->setFlashdata('msg', 'Data has been added');
        return redirect()->route('users_admin');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Users',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'user' => $this->userModel->where('id', $id)->first(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/users/edit', $data);
    }

    public function update($id)
    {

        if (!$this->validate([
            'image' => 'max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_change_users')->withInput();
        }

        $image = $this->request->getFile('image');

        if ($image->getName() == '') {
            $this->userModel->save([
                'id' => $id,
                'id_user_roles' => $this->request->getVar('role'),
                'name' => $this->request->getVar('name'),
                'email' => $this->request->getVar('email'),
            ]);
        } else {
            $image->move('assets/img/users');
            $imageName = $image->getName();

            $this->userModel->save([
                'id' => $id,
                'id_user_roles' => $this->request->getVar('role'),
                'name' => $this->request->getVar('name'),
                'email' => $this->request->getVar('email'),
                'image' => $imageName
            ]);
        }

        $this->session->setFlashdata('msg', 'Data has been edited');
        return redirect()->route('users_admin');
    }

    public function destroy($id)
    {
        $this->userModel->delete($id);
        $this->session->setFlashdata('msg', 'Data has been deleted');
        return redirect()->route('users_admin');
    }
}
