<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\BookingPackageModel;
use App\Models\NotificationModel;
use App\Models\RentalHistoryModel;
use App\Models\UsersModel;

class Rentalhistory extends BaseController
{
    protected $notificationModel;
    protected $historyModel;
    protected $userModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->historyModel = new RentalHistoryModel();
        $this->userModel = new UsersModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Rental Histories',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'histories' => $this->historyModel
                ->select('booking_histories.*, booking_package.booking_package_name, tourist.name AS tourist_name, tour_guide.name AS tour_guide_name')
                ->join('users AS tourist', 'tourist.id=booking_histories.id_user')
                ->join('users AS tour_guide', 'tour_guide.id=booking_histories.id_tour_guide', 'LEFT')
                ->join('booking_package', 'booking_package.id=booking_histories.id_booking_package')
                ->findAll()
        ];

        return view('/pages/admin/rental-history/index', $data);
    }

    public function edit($id)
    {
        $bookingPackageModel = new BookingPackageModel();

        $data = [
            'title' => 'Rental Histories',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'history' => $this->historyModel
                ->select('booking_histories.*, tourist.name AS tourist_name')
                ->join('users AS tourist', 'tourist.id=booking_histories.id_user')
                ->join('users AS tour_guide', 'tour_guide.id=booking_histories.id_tour_guide', 'LEFT')
                ->join('booking_package', 'booking_package.id=booking_histories.id_booking_package')
                ->where('booking_histories.id', $id)
                ->first(),
            'bookingPackages' => $bookingPackageModel->findAll(),
            'tourGuides' => $this->userModel->where('id_user_roles', 3)->findAll(),
            'validation' => \Config\Services::validation()
        ];

        $data['history']['booking_date'] = str_replace(' ', 'T', $data['history']['booking_date']);

        return view('pages/admin/rental-history/edit', $data);
    }

    public function update($id)
    {
        if ($this->request->getVar('tourGuide') == '') {
            $this->historyModel->save([
                'id' => $id,
                'id_booking_package' => $this->request->getVar('bookingPackage'),
                'booking_date' => $this->request->getVar('date'),
                'vehicle' => $this->request->getVar('vehicle'),
                'id_tour_guide' => null
            ]);
        } else {
            $this->historyModel->save([
                'id' => $id,
                'id_booking_package' => $this->request->getVar('bookingPackage'),
                'booking_date' => $this->request->getVar('date'),
                'vehicle' => $this->request->getVar('vehicle'),
                'id_tour_guide' => $this->request->getVar('tourGuide')
            ]);
        }

        $this->session->setFlashdata('msg', 'Data has been edited');
        return redirect()->route('history_admin');
    }

    public function destroy($id)
    {
        $this->historyModel->delete($id);
        $this->session->setFlashdata('msg', 'Data has been deleted');
        return redirect()->route('history_admin');
    }
}
