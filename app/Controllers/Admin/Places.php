<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\NotificationModel;
use App\Models\PlaceModel;

class Places extends BaseController
{
    protected $notificationModel;
    protected $placeModel;

    public function __construct()
    {
        $this->notificationModel = new NotificationModel();
        $this->placeModel = new PlaceModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Places',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'places' => $this->placeModel->findAll()
        ];

        return view('pages/admin/places/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Places',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/places/create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'image' => 'uploaded[image]|max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_add_places')->withInput();
        }

        $image = $this->request->getFile('image');
        $image->move('assets/img/places');
        $imageName = $image->getName();

        $this->placeModel->save([
            'place_name' => $this->request->getVar('placeName'),
            'place_detail' => $this->request->getVar('placeDetail'),
            'image' => $imageName
        ]);

        $this->session->setFlashdata('msg', 'Data has been added');
        return redirect()->route('places_admin');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Places',
            'notifications' => $this->notificationModel->join('users', 'users.id=notifications.id_user')
                ->findAll(),
            'place' => $this->placeModel->where('id', $id)->first(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/admin/places/edit', $data);
    }

    public function update($id)
    {

        if (!$this->validate([
            'image' => 'max_size[image,2048]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png,]'
        ])) {
            return redirect()->route('show_form_change_places')->withInput();
        }

        $image = $this->request->getFile('image');

        if ($image->getName() == '') {
            $this->placeModel->save([
                'id' => $id,
                'place_name' => $this->request->getVar('placeName'),
                'place_detail' => $this->request->getVar('placeDetail')
            ]);
        } else {
            $image->move('assets/img/places');
            $imageName = $image->getName();

            $this->placeModel->save([
                'id' => $id,
                'place_name' => $this->request->getVar('placeName'),
                'place_detail' => $this->request->getVar('placeDetail'),
                'image' => $imageName
            ]);
        }

        $this->session->setFlashdata('msg', 'Data has been edited');
        return redirect()->route('places_admin');
    }

    public function destroy($id)
    {
        $this->placeModel->delete($id);
        $this->session->setFlashdata('msg', 'Data has been deleted');
        return redirect()->route('places_admin');
    }
}
