<?php

namespace App\Controllers;

use App\Models\BookingPackageModel;
use App\Models\CultureModel;
use App\Models\FoodModel;
use App\Models\NotificationModel;
use App\Models\PlaceModel;
use App\Models\RentalHistoryModel;
use App\Models\UsersModel;

class Home extends BaseController
{
	public function index()
	{
		$placeModel = new PlaceModel();
		$cultureModel = new CultureModel();
		$foodModel = new FoodModel();
		$userModel = new UsersModel();
		$bookingPackageModel = new BookingPackageModel();
		$data = [
			'session' => $this->session,
			'bookingPackages' => $bookingPackageModel->findAll(),
			'places' => $placeModel->findAll(),
			'cultures' => $cultureModel->findAll(),
			'foods' => $foodModel->findAll(),
			'users' => $userModel->where('id_user_roles', 3)->findAll()
		];

		return view('pages/home', $data);
	}

	public function book()
	{
		$historyModel = new RentalHistoryModel();
		$notificationModel = new NotificationModel();

		if ($this->request->getVar('bookingpackageselect') == 1) {
			$historyModel->save([
				'id_user' => $this->session->id,
				'id_booking_package' => (int)$this->request->getVar('bookingpackageselect'),
				'booking_date' => $this->request->getVar('date'),
				'vehicle' => $this->request->getVar('vehicleselect'),
				'id_tour_guide' => null
			]);
		} else {
			$historyModel->save([
				'id_user' => $this->session->id,
				'id_booking_package' => (int)$this->request->getVar('bookingpackageselect'),
				'booking_date' => $this->request->getVar('date'),
				'vehicle' => $this->request->getVar('vehicleselect'),
				'id_tour_guide' => (int)$this->request->getVar('tourguideselect')
			]);
		}

		$notificationModel->save([
			'id_user' => $this->session->id,
			'notification_detail' => 'Telah menggunakan jasa pariwisata pada tanggal dan jam berikut: ' . $this->request->getVar('date')
		]);

		$this->session->setFlashdata('msg', 'Sudah dibooking');
		return redirect()->route('home');
	}
}
