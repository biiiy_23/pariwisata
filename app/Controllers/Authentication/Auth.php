<?php

namespace App\Controllers\Authentication;

use App\Controllers\BaseController;
use App\Models\UsersModel;

class Auth extends BaseController
{
    protected $usersModel;
    protected $session;

    public function __construct()
    {
        $this->usersModel = new UsersModel();
        if (session()->get('logged_in')) {
            if (session()->get('user_roles') == 1) {
                return redirect()->route('admin_dashboard');
            } else {
                return redirect()->route('home');
            }
        }
    }

    public function showLoginForm()
    {
        return view('pages/auth/login');
    }

    public function showRegisterForm()
    {
        return view('pages/auth/register');
    }

    public function login()
    {
        $data = [
            'email' => $this->request->getVar('email'),
            'password' => $this->request->getVar('password')
        ];

        // Log in to app

        $user = $this->usersModel
            ->where('email', $data['email'])->first();

        if ($user) {
            $verify_pass = password_verify($data['password'], $user['password']);
            if ($verify_pass) {
                $ses_data = [
                    'id'       => $user['id'],
                    'name'     => $user['name'],
                    'email'    => $user['email'],
                    'user_roles' => (int) $user['id_user_roles'],
                    'logged_in'     => TRUE
                ];
                session()->set($ses_data);
                return redirect()->route('admin_dashboard');
            } else {
                $this->session->setFlashdata('error', 'Wrong Password');
                return redirect()->route('login');
            }
        } else {
            $this->session->setFlashdata('error', 'Email not Found');
            return redirect()->route('login');
        }
    }

    public function register()
    {
        $data = [
            'name' => $this->request->getVar('name'),
            'email' => $this->request->getVar('email'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT)
        ];

        // Check if user has an existing email
        $existingUser = $this->usersModel->where('email', $data['email'])->first();
        if (!$existingUser) {

            // Set user roles
            $data['id_user_roles'] = 2;

            // Register a user
            $user = $this->usersModel->save($data);

            // Set session
            if ($user) {
                $user = $this->usersModel->where('email', $data['email'])->first();
                $ses_data = [
                    'id'       => $user['id'],
                    'name'     => $user['name'],
                    'email'    => $user['email'],
                    'user_roles' => (int) $user['id_user_roles'],
                    'logged_in'     => TRUE
                ];
                $this->session->set($ses_data);

                return redirect()->route('home');
            }
        } else {
            $this->session->setFlashdata('error', 'Email already exists. Please use another email');
            return redirect()->route('register');
        }


        return redirect()->route('home');
    }

    public function logout()
    {
        $this->session->destroy();
        $this->session->setFlashdata('msg', 'Logout Successful');
        return redirect()->route('home');
    }
}
