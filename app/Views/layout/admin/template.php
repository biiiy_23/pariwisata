<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Admin - <?= $title ?></title>
    <!-- Favicon -->
    <link rel="icon" href="/assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <!-- Nucleo -->
    <link rel="stylesheet" href="/assets/css/nucleo.css" type="text/css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="/assets/css/argon.css?v=1.2.0" type="text/css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/custom.css" type="text/css">
</head>

<body>
    <!-- Sidenav -->
    <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        <div class="scrollbar-inner">
            <!-- Brand -->
            <div class="sidenav-header  align-items-center">
                <a class="navbar-brand" href="<?= route_to('admin_dashboard') ?>">
                    <img src="/assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
                </a>
            </div>
            <div class="navbar-inner">
                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                    <!-- Nav items -->
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <?php if (current_url() == 'http://localhost:8080/admin') : ?>
                                <a class="nav-link active" href="<?= route_to('admin_dashboard') ?>">
                                    <i class="ni ni-tv-2 text-orange"></i>
                                    <span class="nav-link-text">Dashboard</span>
                                </a>
                            <?php else : ?>
                                <a class="nav-link" href="<?= route_to('admin_dashboard') ?>">
                                    <i class="ni ni-tv-2 text-primary"></i>
                                    <span class="nav-link-text">Dashboard</span>
                                </a>
                            <?php endif; ?>
                        </li>
                        <li class="nav-item">
                            <?php if (current_url() == 'http://localhost:8080/admin/users') : ?>
                                <a class="nav-link active" href="<?= route_to('users_admin') ?>">
                                    <i class="ni ni-badge text-orange"></i>
                                    <span class="nav-link-text">Users</span>
                                </a>
                            <?php else : ?>
                                <a class="nav-link" href="<?= route_to('users_admin') ?>">
                                    <i class="ni ni-badge text-primary"></i>
                                    <span class="nav-link-text">Users</span>
                                </a>
                            <?php endif; ?>
                        </li>
                        <li class="nav-item">
                            <?php if (current_url() == 'http://localhost:8080/admin/rental-history') : ?>
                                <a class="nav-link active" href="<?= route_to('history_admin') ?>">
                                    <i class="ni ni-collection text-orange"></i>
                                    <span class="nav-link-text">Rental history</span>
                                </a>
                            <?php else : ?>
                                <a class="nav-link" href="<?= route_to('history_admin') ?>">
                                    <i class="ni ni-collection text-primary"></i>
                                    <span class="nav-link-text">Rental history</span>
                                </a>
                            <?php endif; ?>
                        </li>
                        <li class="sidebar-dropdown">
                            <a class="nav-link nav-menu" href="#">
                                <i class="ni ni-world text-primary"></i>
                                <span class="nav-link-text">Destinations</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li class="nav-item">
                                        <?php if (current_url() == 'http://localhost:8080/admin/cultures') : ?>
                                            <a class="nav-link active" href="<?= route_to('cultures_admin') ?>">
                                                <i class="ni ni-atom text-orange"></i>
                                                <span class="nav-sublink-text">Cultures</span>
                                            </a>
                                        <?php else : ?>
                                            <a class="nav-link" href="<?= route_to('cultures_admin') ?>">
                                                <i class="ni ni-atom text-primary"></i>
                                                <span class="nav-sublink-text">Cultures</span>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                    <li class="nav-item">
                                        <?php if (current_url() == 'http://localhost:8080/admin/foods') : ?>
                                            <a class="nav-link active" href="<?= route_to('foods_admin') ?>">
                                                <i class="ni ni-shop text-orange"></i>
                                                <span class="nav-sublink-text">Foods</span>
                                            </a>
                                        <?php else : ?>
                                            <a class="nav-link" href="<?= route_to('foods_admin') ?>">
                                                <i class="ni ni-shop text-primary"></i>
                                                <span class="nav-sublink-text">Foods</span>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                    <li class="nav-item">
                                        <?php if (current_url() == 'http://localhost:8080/admin/places') : ?>
                                            <a class="nav-link active" href="<?= route_to('places_admin') ?>">
                                                <i class="ni ni-pin-3 text-orange"></i>
                                                <span class="nav-sublink-text">Places</span>
                                            </a>
                                        <?php else : ?>
                                            <a class="nav-link" href="<?= route_to('places_admin') ?>">
                                                <i class="ni ni-pin-3 text-primary"></i>
                                                <span class="nav-sublink-text">Places</span>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <?php if (current_url() == 'http://localhost:8080/admin/notifications') : ?>
                                <a class="nav-link active" href="<?= route_to('notifications_admin') ?>">
                                    <i class="ni ni-bell-55 text-orange"></i>
                                    <span class="nav-link-text">Notifications</span>
                                </a>
                            <?php else : ?>
                                <a class="nav-link" href="<?= route_to('notifications_admin') ?>">
                                    <i class="ni ni-bell-55 text-primary"></i>
                                    <span class="nav-link-text">Notifications</span>
                                </a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Search form -->
                    <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                        <div class="form-group mb-0">
                            <div class="input-group input-group-alternative input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input class="form-control" placeholder="Search" type="text">
                            </div>
                        </div>
                        <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </form>
                    <!-- Navbar links -->
                    <ul class="navbar-nav align-items-center  ml-md-auto ">
                        <li class="nav-item d-xl-none">
                            <!-- Sidenav toggler -->
                            <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                                <div class="sidenav-toggler-inner">
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item d-sm-none">
                            <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                                <i class="ni ni-zoom-split-in"></i>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ni ni-bell-55"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                                <!-- Dropdown header -->
                                <div class="px-3 py-3">
                                    <h6 class="text-sm text-muted m-0">You have <strong class="text-primary"><?= count($notifications) ?></strong> notifications.</h6>
                                </div>
                                <!-- List group -->
                                <div class="list-group list-group-flush">
                                    <?php foreach ($notifications as $notif) : ?>
                                        <?php if ($notif['status'] != 'Read') : ?>
                                            <a href="<?= route_to('notifications_admin') ?>" class="list-group-item list-group-item-action">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <!-- Avatar -->
                                                        <img alt="Image placeholder" src="/assets/img/users/<?= $notif['image'] ?>" class="avatar rounded-circle">
                                                    </div>
                                                    <div class="col ml--2">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div>
                                                                <h4 class="mb-0 text-sm"><?= $notif['name'] ?></h4>
                                                            </div>
                                                        </div>
                                                        <p class="text-sm mb-0"><?= $notif['notification_detail'] ?></p>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </div>
                                <!-- View all -->
                                <a href="<?= route_to('notifications_admin') ?>" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                        <li class="nav-item dropdown">
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle">
                                        <img alt="Image placeholder" src="/assets/img/theme/team-4.jpg">
                                    </span>
                                    <div class="media-body  ml-2  d-none d-lg-block">
                                        <span class="mb-0 text-sm  font-weight-bold">Admin</span>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu  dropdown-menu-right ">
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome!</h6>
                                </div>
                                <a href="#!" class="dropdown-item">
                                    <i class="ni ni-single-02"></i>
                                    <span>My profile</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= route_to('logout') ?>" class="dropdown-item">
                                    <i class="ni ni-user-run"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <?php if (current_url() == 'http://localhost:8080/admin') : ?>
            <div class="header bg-primary pb-6">
                <div class="container-fluid">
                    <div class="header-body">
                        <div class="row align-items-center py-4">
                            <div class="col-lg-6 col-7">
                                <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
                            </div>
                        </div>
                        <!-- Card stats -->
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                                <a class="link" href="<?= route_to('users_admin') ?>">
                                    <div class="card card-stats">
                                        <!-- Card body -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Tourist</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?= count($tourist) ?> Orang</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                                        <i class="ni ni-chart-pie-35"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <a class="link" href="<?= route_to('users_admin') ?>">
                                    <div class="card card-stats">
                                        <!-- Card body -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Tour Guide</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?= count($tourGuides) ?> Orang</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                                        <i class="ni ni-chart-pie-35"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <a class="link" href="<?= route_to('history_admin') ?>">
                                    <div class="card card-stats">
                                        <!-- Card body -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Rental history</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?= count($histories) ?></span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                                        <i class="ni ni-money-coins"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="header bg-primary pb-6">
                <div class="container-fluid">
                    <div class="header-body">
                        <div class="row align-items-center py-4">
                            <div class="col-lg-6 col-7">
                                <h6 class="h2 text-white d-inline-block mb-0"><?= $title ?></h6>
                                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                        <li class="breadcrumb-item"><a href="<?= route_to('admin_dashboard') ?>"><i class="fas fa-home"></i></a></li>
                                        <li class="breadcrumb-item active" aria-current="page"><?= $title ?></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- Page content -->
        <?= $this->renderSection('content'); ?>
    </div>


    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/js.cookie.js"></script>
    <script src="/assets/js/jquery.scrollbar.min.js"></script>
    <script src="/assets/js/jquery-scrollLock.min.js"></script>
    <!-- Argon JS -->
    <script src="/assets/js/argon.js?v=1.2.0"></script>
    <!-- Custom JS -->
    <script src="/assets/js/custom.js"></script>
</body>

</html>