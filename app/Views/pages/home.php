<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="Aria is a business focused free HTML landing page template built with Bootstrap to help you create lead generation websites for companies and their services.">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="" />
    <!-- website name -->
    <meta property="og:site" content="" />
    <!-- website link -->
    <meta property="og:title" content="" />
    <!-- title shown in the actual shared post -->
    <meta property="og:description" content="" />
    <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" />
    <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" />
    <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>Aria</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Nucleo -->
    <link rel="stylesheet" href="assets/css/nucleo.css" type="text/css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="assets/css/all.min.css" type="text/css">
    <link href="/assets/css/all.min.css" rel="stylesheet">
    <link href="/assets/css/swiper.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">
    <link href="/assets/css/home.css" rel="stylesheet">

    <!-- Favicon  -->
    <link rel="icon" href="../assets/img/brand/favicon2.png">
</head>

<body data-spy="scroll" data-target=".fixed-top">
    <!-- Preloader -->
    <div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->


    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Aria</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="<?= route_to('home') ?>"><img src="../assets/img/brand/logo.svg" alt="alternative"></a>

        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <?php if ($session->get('logged_in') == null) : ?>
                        <a class="nav-link page-scroll" href="<?= route_to('show_login_form') ?>">LOGIN</a>
                    <?php else : ?>
                        <?php if ($session->get('user_roles') == 1) : ?>
                            <a class="nav-link page-scroll text-uppercase" href="<?= route_to('admin_dashboard') ?>"><?= $session->name ?></a>
                        <?php elseif ($session->get('user_roles') == 2) : ?>
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <div class="media-body ml-2 d-none d-lg-flex">
                                        <i class="ni ni-single-02 mr-2"></i>
                                        <span class="mb-0 text-sm text-uppercase"><?= $session->name ?></span>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu">
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow text-white">Welcome!</h6>
                                </div>
                                <a href="#!" class="dropdown-item">
                                    <i class="ni ni-single-02"></i>
                                    <span>My profile</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= route_to('logout') ?>" class="dropdown-item">
                                    <i class="ni ni-user-run"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </nav>
    <!-- end of navbar -->
    <!-- end of navbar -->

    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <!-- Flash Data After Log Out -->
                <?php if (session()->getFlashdata('msg')) : ?>
                    <div class="alert alert-success alert-dismissible fade show mx-5" role="alert">
                        <strong>Log</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-container">
                            <h1><span id="js-rotating">JASA, SOLUSI</span> PARIWISATA</h1>
                            <p class="p-heading p-large">Aria adalah perusahaan konsultan terkemuka yang berspesialisasi dalam pertumbuhan bisnis menggunakan pemasaran online</p>
                            <a class="btn-solid-lg page-scroll" href="#intro">DISCOVER</a>
                        </div>
                    </div>
                    <!-- end of col -->
                </div>
                <!-- end of row -->
            </div>
            <!-- end of container -->
        </div>
        <!-- end of header-content -->
    </header>
    <!-- end of header -->


    <!-- Intro -->
    <div id="intro" class="basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="text-container">
                        <div class="section-title">INTRO</div>
                        <h2>Kami Menawarkan Beberapa Layanan Pariwisata Terbaik Di Kota</h2>
                        <p>Mencari tempat wisata dengan kebudayaan, makanan dan tempat yang unik menjadi proses yang sangat melelahkan.</p>
                        <p class="testimonial-text">"Misi kami di Aira adalah untuk membantu Anda melewati saat-saat sulit dengan mengandalkan keahlian tim kami dalam memulai dan menjelajahi tempat-tempat pariwisata."</p>
                        <div class="testimonial-author">Sulaiman - CEO</div>
                    </div>
                    <!-- end of text-container -->
                </div>
                <!-- end of col -->
                <div class="col-lg-7">
                    <div class="image-container">
                        <img class="img-fluid" src="../assets/img/background/intro-office.jpg" alt="alternative">
                    </div>
                    <!-- end of image-container -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of intro -->

    <!-- Services -->
    <div id="services" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">LAYANAN</div>
                    <h2>Pilih Paket Layanan <br> Sesuai Kebutuhan Anda</h2>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <?php $i = 1; ?>
                    <?php foreach ($bookingPackages as $bp) : ?>
                        <!-- Card -->
                        <div class="card">
                            <div class="card-image">
                                <img class="img-fluid" src="assets/img/background/<?= $bp['image'] ?>" alt="alternative">
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"><?= $bp['booking_package_name'] ?></h3>
                                <p><?= $bp['booking_package_detail'] ?></p>
                                <?php if ($bp['booking_package_name'] == 'Paket 1') : ?>
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Dapat menggunakan kendaraan roda 2 atau 4</div>
                                        </li>
                                    </ul>
                                <?php else : ?>
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Dapat menggunakan kendaraan roda 2 atau 4</div>
                                        </li>
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Ditemani oleh tour guide</div>
                                        </li>
                                    </ul>
                                <?php endif ?>
                                <p class="price">Dimulai dari <span>Rp. <?= $bp['price'] ?></span></p>
                            </div>
                            <div class="button-container">
                                <?php if ($session->get('logged_in') == null) : ?>
                                    <a class="btn-solid-reg page-scroll" href="<?= route_to('login') ?>">DETAILS</a>
                                <?php else : ?>
                                    <?php if ($session->get('user_roles') == 2) : ?>
                                        <button class="btn-solid-reg page-scroll btn-book-pk-<?= $i ?>" data-toggle="modal" data-target="#bookedModal">DETAILS</button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <!-- end of button-container -->
                        </div>
                        <!-- end of card -->
                        <?php $i++ ?>
                    <?php endforeach ?>

                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of services -->

    <!-- Modal Services -->
    <div class="modal fade" id="bookedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="form" action="<?= route_to('book') ?>" method="POST">
                <?= csrf_field() ?>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Booking Form</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control-select" id="bookingpackageselect" name="bookingpackageselect" required>
                                <option class="select-option" value="" disabled selected>Silahkan pilih paket...</option>
                                <?php foreach ($bookingPackages as $bp) : ?>
                                    <option class="select-option" value="<?= $bp['id'] ?>"><?= $bp['booking_package_name'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="datetime-local" class="form-control-input date-input" id="date" name="date" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <select class="form-control-select" id="vehicleselect" name="vehicleselect" required>
                                <option class="select-option" value="" disabled selected>Silahkan pilih kendaraan...</option>
                                <option class="select-option" value="Mobil">Mobil</option>
                                <option class="select-option" value="Motor">Motor</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group form-tour-guide">
                            <select class="form-control-select" id="tourguideselect" name="tourguideselect">
                                <option class="select-option" value="" disabled selected>Silahkan pilih tour guide...</option>
                                <?php foreach ($users as $user) : ?>
                                    <?php if ($user['id_user_roles'] == 3) : ?>
                                        <option class="select-option" value="<?= $user['id'] ?>"><?= $user['name'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            <small id="emailHelp" class="form-text text-info">*Kolom tour guide tidak harus dipilih</small>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-reg" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-solid-reg">Book</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- end of modal services -->

    <!-- Places -->
    <div id="projects" class="filter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">TEMPAT</div>
                    <h2>Tempat Yang Kami Banggakan</h2>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- end of button group -->
                    <div class="grid">
                        <?php $i = 0; ?>
                        <?php foreach ($places as $place) : ?>
                            <div class="element-item development">
                                <a class="popup-with-move-anim" href="#place-<?= $i++ ?>">
                                    <div class="element-item-overlay"><span><?= $place['place_name'] ?></span></div><img src="/assets/img/places/<?= $place['image'] ?>" alt="alternative">
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <!-- end of grid -->
                    <!-- end of filter -->

                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of filter -->
    <!-- end of projects -->

    <!-- Lightbox Places -->
    <?php $i = 0; ?>
    <?php foreach ($places as $place) : ?>
        <div id="place-<?= $i++ ?>" class="lightbox-basic zoom-anim-dialog mfp-hide">
            <div class="row">
                <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
                <div class="col-lg-8">
                    <img class="img-fluid" src="/assets/img/places/<?= $place['image'] ?>" alt="alternative">
                </div>
                <!-- end of col -->
                <div class="col-lg-4">
                    <h3><?= $place['place_name'] ?></h3>
                    <hr class="line-heading">
                    <p><?= $place['place_detail'] ?></p>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
    <?php endforeach ?>
    <!-- end of lightbox-basic -->
    <!-- end of lightbox -->


    <!-- Foods -->
    <div class="slider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-4">
                    <div class="section-title text-center mb-2">MAKANAN</div>
                    <h2>Makanan yang ada di tempat kami</h2>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Card Slider -->
                    <div class="slider-container">
                        <div class="swiper-container card-slider">
                            <div class="swiper-wrapper">
                                <?php foreach ($foods as $food) : ?>
                                    <!-- Slide -->
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <img class="card-image" src="/assets/img/foods/<?= $food['image'] ?>" alt="alternative">
                                            <div class="card-body">
                                                <div class="testimonial-text"><?= $food['food_detail'] ?></div>
                                                <div class="testimonial-author"><?= $food['food_name'] ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end of swiper-slide -->
                                    <!-- end of slide -->
                                <?php endforeach ?>

                            </div>
                            <!-- end of swiper-wrapper -->

                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->

                        </div>
                        <!-- end of swiper-container -->
                    </div>
                    <!-- end of sliedr-container -->
                    <!-- end of card slider -->

                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of slider -->
    <!-- end of testimonials -->


    <!-- Cultures -->
    <div id="projects" class="filter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">BUDAYA</div>
                    <h2>Budaya yang ada</h2>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- end of button group -->
                    <div class="grid">
                        <?php $i = 0; ?>
                        <?php foreach ($cultures as $culture) : ?>
                            <div class="element-item development">
                                <a class="popup-with-move-anim" href="#culture-<?= $i++ ?>">
                                    <div class="element-item-overlay"><span><?= $culture['culture_name'] ?></span></div><img src="/assets/img/cultures/<?= $culture['image'] ?>" alt="alternative">
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <!-- end of grid -->
                    <!-- end of filter -->

                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of filter -->
    <!-- end of projects -->

    <!-- Lightbox Cultures -->
    <?php $i = 0; ?>
    <?php foreach ($cultures as $culture) : ?>
        <div id="culture-<?= $i++ ?>" class="lightbox-basic zoom-anim-dialog mfp-hide">
            <div class="row">
                <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
                <div class="col-lg-8">
                    <img class="img-fluid" src="/assets/img/cultures/<?= $culture['image'] ?>" alt="alternative">
                </div>
                <!-- end of col -->
                <div class="col-lg-4">
                    <h3><?= $culture['culture_name'] ?></h3>
                    <hr class="line-heading">
                    <p><?= $culture['culture_detail'] ?></p>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
    <?php endforeach ?>
    <!-- end of lightbox-basic -->
    <!-- end of lightbox -->

    <!-- Team -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Tim Pemandu Kami</h2>
                    <p class="p-heading">Kami hanya sekuat dan sepengetahuan tim kami. Jadi, inilah pria dan wanita yang membantu anda mencapai tujuan dalam menjelajahi destinasi</p>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <?php foreach ($users as $user) : ?>
                        <!-- Team Member -->
                        <div class="team-member">
                            <div class="image-wrapper">
                                <img class="img-fluid" src="/assets/img/teams/<?= $user['image'] ?>" alt="alternative">
                            </div>
                            <!-- end of image-wrapper -->
                            <p class="p-large"><?= $user['name'] ?></p>
                            <span class="social-icons">
                                <span class="fa-stack">
                                    <a href="#your-link">
                                        <span class="hexagon"></span>
                                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                                    </a>
                                </span>
                                <span class="fa-stack">
                                    <a href="#your-link">
                                        <span class="hexagon"></span>
                                        <i class="fab fa-twitter fa-stack-1x"></i>
                                    </a>
                                </span>
                            </span>
                        </div>
                        <!-- end of team-member -->
                        <!-- end of team member -->
                    <?php endforeach ?>

                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of basic-2 -->
    <!-- end of team -->

    <!-- Footer -->
    <div class="footer pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="text-center">
                        <h4>Sedikit Kata Tentang Aria</h4>
                        <p class="white">Kami bersemangat memberikan layanan pariwisata terbaik untuk para turis yang ingin menjelajahi destinasi yang diinginkan sejak lama.</p>
                    </div>
                    <!-- end of text-container -->
                </div>
                <div class="col-md-2">
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of footer -->
    <!-- end of footer -->


    <!-- Scripts -->
    <script src="/assets/js/jquery.min.js"></script>
    <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="/assets/js/popper.min.js"></script>
    <!-- Popper tooltip library for Bootstrap -->
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap framework -->
    <script src="/assets/js/jquery.easing.min.js"></script>
    <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="/assets/js/swiper.min.js"></script>
    <!-- Swiper for image and text sliders -->
    <script src="/assets/js/jquery.magnific-popup.js"></script>
    <!-- Magnific Popup for lightboxes -->
    <script src="/assets/js/morphext.min.js"></script>
    <!-- Morphtext rotating text in the header -->
    <script src="/assets/js/isotope.pkgd.min.js"></script>
    <!-- Isotope for filter -->
    <script src="/assets/js/validator.min.js"></script>
    <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="/assets/js/scripts.js"></script>
    <!-- Argon JS -->
    <script src="/assets/js/argon.js?v=1.2.0"></script>
    <!-- Custom JS -->
    <script src="/assets/js/custom.js"></script>
    <!-- Custom scripts -->
</body>

</html>