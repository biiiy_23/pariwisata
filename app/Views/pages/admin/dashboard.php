<?= $this->extend('layout/admin/template'); ?>

<?= $this->section('content');  ?>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="mb-0">Rental History</h3>
                        </div>
                        <div class="col-6">
                            <?php $now = date('d M Y'); ?>
                            <h3 class="mb-0 text-right"><?= $now ?></h3>
                        </div>
                    </div>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tourist</th>
                                <th scope="col">Booking Package</th>
                                <th scope="col">Tour Guide</th>
                                <th scope="col">Vehicle</th>
                                <th scope="col">Date and Time</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php if (empty($currentHistory)) : ?>
                                <tr>
                                    <th scope="row" class="text-uppercase text-center" colspan="6">You have no history</th>
                                </tr>
                            <?php else : ?>
                                <?php $i = 1 ?>
                                <?php foreach ($currentHistory as $history) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <th scope="row"><?= $history['id_user'] ?></th>
                                        <th scope="row"><?= $history['booking_package_name'] ?></th>
                                        <th scope="row"><?= $history['id_tour_guide'] == null ? '-' : $history['id_tour_guide'] ?></th>
                                        <th><?= $history['vehicle'] ?></th>
                                        <th><?= $history['booking_date'] ?></th>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
                <!-- Card footer -->
                <div class="card-footer py-4">
                    <!-- <nav aria-label="...">
                        <ul class="pagination justify-content-end mb-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">
                                    <i class="fas fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="fas fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection();  ?>