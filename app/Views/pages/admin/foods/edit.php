<?= $this->extend('layout/admin/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <form class="form" action="<?= route_to('edit_foods', $food['id']) ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field() ?>
                <input type="hidden" name="id" value="<?= $food['id'] ?>">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Edit Data Makanan</h3>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <!-- Form -->
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="foodName" placeholder="name" value="<?= $food['food_name'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="detail">Detail</label>
                            <textarea class="form-control" id="detail" name="foodDetail" rows="3"><?= $food['food_detail'] ?></textarea>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Image</label>
                        </div>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer text-right py-4">
                        <a class="btn btn-secondary" href="<?= route_to('foods_admin') ?>">Back</a>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?= $this->endSection() ?>