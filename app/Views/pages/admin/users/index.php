<?= $this->extend('layout/admin/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt--6">
    <?php if (session()->getFlashdata('msg')) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
            <span class="alert-text"><strong>Success!</strong> <?= session()->getFlashdata('msg') ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="mb-0">Users</h3>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a class="btn btn-primary" href="<?= route_to('show_form_add_users') ?>">Tambah User</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Role</th>
                                <th scope="col">Joined At</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php $i = 1; ?>
                            <?php foreach ($users as $user) : ?>
                                <?php if ($user['id_user_roles'] != 1) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <th scope="row"><?= $user['name'] ?></th>
                                        <td><?= $user['id_user_roles'] == 2 ? 'Tourist' : 'Tour Guide' ?></td>
                                        <td>
                                            <?php
                                            $time = strtotime($user['created_at']);
                                            $user['created_at'] = date('d M Y', $time);
                                            echo $user['created_at'];
                                            ?>
                                        </td>
                                        <td>
                                            <span class="badge badge-dot mr-4">
                                                <?php if ($user['deleted_at'] == null) : ?>
                                                    <i class="bg-success"></i>
                                                    <span class="status">Active</span>
                                                <?php else : ?>
                                                    <i class="bg-danger"></i>
                                                    <span class="status">Not Active</span>
                                                <?php endif; ?>
                                            </span>
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="<?= route_to('show_form_change_users', $user['id']) ?>">Change</a>
                                                    <form action="<?= route_to('delete_users', $user['id']) ?>" method="post">
                                                        <?= csrf_field() ?>
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="dropdown-item" onclick="return confirm('Do you want to delete this data?');">Delete</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <!-- Card footer -->
                <div class="card-footer py-4">
                    <!-- <nav aria-label="...">
                        <ul class="pagination justify-content-end mb-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">
                                    <i class="fas fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="fas fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>