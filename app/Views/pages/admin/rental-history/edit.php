<?= $this->extend('layout/admin/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <form class="form" action="<?= route_to('edit_history', $history['id']) ?>" method="post">
                <?= csrf_field() ?>
                <input type="hidden" name="id" value="<?= $history['id'] ?>">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Edit Data History</h3>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <!-- Form -->
                        <div class="form-group">
                            <label for="name">Tourist Name</label>
                            <input type="text" class="form-control" id="name" value="<?= $history['tourist_name'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="booking-package">Booking Package</label>
                            <select class="form-control" id="booking-package" name="bookingPackage">
                                <option value="" disabled>Select Booking Package...</option>
                                <?php if ($history['id_booking_package'] == 1) : ?>
                                    <option value="1" selected>Paket 1</option>
                                    <option value="2">Paket 2</option>
                                <?php elseif ($history['id_booking_package'] == 1) : ?>
                                    <option value="1">Paket 1</option>
                                    <option value="2" selected>Paket 2</option>
                                <?php endif ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="booking-date" class="form-control-label">Booking Date</label>
                            <input class="form-control" type="datetime-local" id="booking-date" name="date" value="<?= $history['booking_date'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="vehicle">Vehicle</label>
                            <select class="form-control" id="vehicle" name="vehicle">
                                <option value="" disabled>Select vehicle...</option>
                                <?php if ($history['vehicle'] == 'Motor') : ?>
                                    <option value="Motor" selected>Motor</option>
                                    <option value="Mobil">Mobil</option>
                                <?php else : ?>
                                    <option value="Motor">Motor</option>
                                    <option value="Mobil" selected>Mobil</option>
                                <?php endif ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tour-guide">Tour Guide</label>
                            <select class="form-control" id="tour-guide" name="tourGuide">
                                <?php if ($history['id_tour_guide'] == null) : ?>
                                    <option value="" selected>Select tour guide...</option>
                                    <?php foreach ($tourGuides as $tg) : ?>
                                        <option value="<?= $tg['id'] ?>"><?= $tg['name'] ?></option>
                                    <?php endforeach ?>
                                <?php else : ?>
                                    <option value="">Select tour guide...</option>
                                    <?php foreach ($tourGuides as $tg) : ?>
                                        <?php if ($tg['id'] == $history['id_tour_guide']) : ?>
                                            <option value="<?= $tg['id'] ?>" selected><?= $tg['name'] ?></option>
                                        <?php else : ?>
                                            <option value="<?= $tg['id'] ?>"><?= $tg['name'] ?></option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer text-right py-4">
                        <a class="btn btn-secondary" href="<?= route_to('history_admin') ?>">Back</a>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?= $this->endSection() ?>