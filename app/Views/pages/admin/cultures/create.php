<?= $this->extend('layout/admin/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <form class="form" action="<?= route_to('add_cultures') ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field() ?>
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Tambah Data Culture</h3>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <!-- Form -->
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="cultureName" placeholder="name" value="<?= old('cultureName') ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="detail">Detail</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="cultureDetail" rows="3"><?= old('cultureDetail') ?></textarea>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image" required>
                            <label class="custom-file-label" for="image">Image</label>
                        </div>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer text-right py-4">
                        <a class="btn btn-secondary" href="<?= route_to('cultures_admin') ?>">Back</a>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?= $this->endSection() ?>