<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// Home
$routes->get('/', 'Home::index', ['as' => 'home']);
$routes->post('/book', 'Home::book', ['as' => 'book']);

// Authentication
$routes->get('/login', 'Authentication\Auth::showLoginForm', ['as' => 'show_login_form']);
$routes->post('/login', 'Authentication\Auth::login', ['as' => 'login']);
$routes->get('/register', 'Authentication\Auth::showRegisterForm', ['as' => 'show_register_form']);
$routes->post('/register', 'Authentication\Auth::register', ['as' => 'register']);
$routes->get('/logout', 'Authentication\Auth::logout', ['as' => 'logout']);

// Admin
$routes->group('admin', ['namespace' => APP_NAMESPACE . "\Controllers\Admin", 'filter' => 'auth'],  function ($routes) {
	// Dashboard
	$routes->get('', 'Home::index', ['as' => 'admin_dashboard']);

	// Users
	$routes->get('users', 'Users::index', ['as' => 'users_admin']);
	$routes->get('users/add', 'Users::create', ['as' => 'show_form_add_users']);
	$routes->post('users/add', 'Users::store', ['as' => 'add_users']);
	$routes->get('users/edit/(:num)', 'Users::edit/$1', ['as' => 'show_form_change_users']);
	$routes->post('users/edit/(:num)', 'Users::update/$1', ['as' => 'edit_users']);
	$routes->delete('users/delete/(:num)', 'Users::destroy/$1', ['as' => 'delete_users']);

	// Rental History
	$routes->get('rental-history', 'Rentalhistory::index', ['as' => 'history_admin']);
	$routes->get('rental-history/edit/(:num)', 'Rentalhistory::edit/$1', ['as' => 'show_form_change_history']);
	$routes->post('rental-history/edit/(:num)', 'Rentalhistory::update/$1', ['as' => 'edit_history']);
	$routes->delete('rental-history/delete/(:num)', 'Rentalhistory::destroy/$1', ['as' => 'delete_history']);

	// Cultures
	$routes->get('cultures', 'Cultures::index', ['as' => 'cultures_admin']);
	$routes->get('cultures/add', 'Cultures::create', ['as' => 'show_form_add_cultures']);
	$routes->post('cultures/add', 'Cultures::store', ['as' => 'add_cultures']);
	$routes->get('cultures/edit/(:num)', 'Cultures::edit/$1', ['as' => 'show_form_change_cultures']);
	$routes->post('cultures/edit/(:num)', 'Cultures::update/$1', ['as' => 'edit_cultures']);
	$routes->delete('cultures/delete/(:num)', 'Cultures::destroy/$1', ['as' => 'delete_cultures']);

	// Foods
	$routes->get('foods', 'Foods::index', ['as' => 'foods_admin']);
	$routes->get('foods/add', 'Foods::create', ['as' => 'show_form_add_foods']);
	$routes->post('foods/add', 'Foods::store', ['as' => 'add_foods']);
	$routes->get('foods/edit/(:num)', 'Foods::edit/$1', ['as' => 'show_form_change_foods']);
	$routes->post('foods/edit/(:num)', 'Foods::update/$1', ['as' => 'edit_foods']);
	$routes->delete('foods/delete/(:num)', 'Foods::destroy/$1', ['as' => 'delete_foods']);

	// Places
	$routes->get('places', 'Places::index', ['as' => 'places_admin']);
	$routes->get('places/add', 'Places::create', ['as' => 'show_form_add_places']);
	$routes->post('places/add', 'Places::store', ['as' => 'add_places']);
	$routes->get('places/edit/(:num)', 'Places::edit/$1', ['as' => 'show_form_change_places']);
	$routes->post('places/edit/(:num)', 'Places::update/$1', ['as' => 'edit_places']);
	$routes->delete('places/delete/(:num)', 'Places::destroy/$1', ['as' => 'delete_places']);

	// Notifications
	$routes->get('notifications', 'Notifications::index', ['as' => 'notifications_admin']);
});



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
