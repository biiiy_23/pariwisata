-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: pariwisata
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking_histories`
--

DROP TABLE IF EXISTS `booking_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_histories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) unsigned NOT NULL,
  `id_booking_package` bigint(20) unsigned NOT NULL,
  `booking_date` datetime NOT NULL,
  `vehicle` enum('Motor','Mobil') DEFAULT NULL,
  `id_tour_guide` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_booking_histories_users` (`id_user`),
  KEY `FK_booking_histories_booking_package` (`id_booking_package`),
  KEY `FK_booking_histories_users_2` (`id_tour_guide`),
  CONSTRAINT `FK_booking_histories_booking_package` FOREIGN KEY (`id_booking_package`) REFERENCES `booking_package` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_booking_histories_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_booking_histories_users_2` FOREIGN KEY (`id_tour_guide`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_histories`
--

LOCK TABLES `booking_histories` WRITE;
/*!40000 ALTER TABLE `booking_histories` DISABLE KEYS */;
INSERT INTO `booking_histories` VALUES (1,2,1,'2021-07-22 15:26:27','Motor',3,'2021-07-22 15:26:29','2021-07-25 13:04:24',NULL),(2,2,2,'2021-07-24 10:45:00','Mobil',NULL,'2021-07-23 22:46:32','2021-07-25 12:18:18',NULL);
/*!40000 ALTER TABLE `booking_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_package`
--

DROP TABLE IF EXISTS `booking_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_package` (
  `id` bigint(2) unsigned NOT NULL AUTO_INCREMENT,
  `booking_package_name` varchar(64) NOT NULL,
  `booking_package_detail` text NOT NULL,
  `price` int(12) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_package`
--

LOCK TABLES `booking_package` WRITE;
/*!40000 ALTER TABLE `booking_package` DISABLE KEYS */;
INSERT INTO `booking_package` VALUES (1,'Paket 1','Jelajahi tempat wisata menggunakan kendaraan roda 2 ataupun roda 4 sendirian',500000,'services-1.jpg','2021-07-21 16:05:57','2021-07-21 16:05:58',NULL),(2,'Paket 2','Jelajahi tempat wisata menggunakan kendaraan roda 2 ataupun roda 4 dengan ditemani oleh tour guide yang ramah',1000000,'services-2.jpg','2021-07-21 16:05:59','2021-07-21 16:05:59',NULL);
/*!40000 ALTER TABLE `booking_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cultures`
--

DROP TABLE IF EXISTS `cultures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cultures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `culture_name` varchar(64) NOT NULL,
  `culture_detail` text NOT NULL,
  `image` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cultures`
--

LOCK TABLES `cultures` WRITE;
/*!40000 ALTER TABLE `cultures` DISABLE KEYS */;
INSERT INTO `cultures` VALUES (1,'Cowongan','Cowongan adalah salah satu kebudayaan banyumas yang merupakan jenis ritual atau upacara minta hujan yang dilakukan oleh masyarakat di daerah Banyumas dan sekitarnya. Menurut kepercayaan masyarakat Banyumas, permintaan datangnya hujan melalui cowongan, dilakukan dengan bantuan bidadari, Dewi Sri yang merupakan dewi padi, lambang kemakmuran dan kesejahteraan.','cowongan-min.jpg','2021-07-21 16:38:41','2021-07-21 16:38:41',NULL),(2,'Ebeg',' Ebeg adalah bentuk tari tradisional khas Banyumas dengan properti utama berupa ebeg atau kuda kepang. Kesenian ini menggambarkan kegagahan prajurit berkuda dengan segala atraksinya.','ebeg-min.jpg','2021-07-21 16:39:41','2021-07-21 16:39:41',NULL),(3,'Begalan','Begalan ialah ritual yang merupakan bagian dari rangkaian upacara pernikahan yang terdapat di Kabupaten Banyumas, Purbalingga, maupun Banjarnegara, Jawa Tengah. Kata “begalan” jika diterjemahkan ke dalam bahasa Indonesia berarti perampokan.','begalan.jpg','2021-07-21 16:40:35','2021-07-25 08:28:10',NULL),(5,'ASD','ASDasasa','','2021-07-25 08:43:03','2021-07-25 08:43:03',NULL);
/*!40000 ALTER TABLE `cultures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_name` varchar(64) NOT NULL,
  `food_detail` text NOT NULL,
  `image` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foods`
--

LOCK TABLES `foods` WRITE;
/*!40000 ALTER TABLE `foods` DISABLE KEYS */;
INSERT INTO `foods` VALUES (1,'Soto Sokaraja','Soto Sokaraja ini sebenarnya sama dengan soto pada umumnya, yang membedakan dan menjadi ciri khas kuliner ini adalah penggunaan ketupat dan sambal kacang. Selain itu kerupuk yang digunakan adalah kerupuk canthir dari bahan ketela pohon yang juga mempunyai rasa khas.','soto_sokaraja.jpg','2021-07-22 14:47:43','2021-07-22 14:47:43',NULL),(2,'Klanting atau Lanting Bumbu','Makanan khas Purwokerto Banyumas ini disebut klanting karena bentuknya mirip dengan anting. Makanan ini terbuat dari bahan singkong yang diparut dan diperas airnya kemudin dicampur dengan bumbu-bumbu lalu digoreng hingga berubah warna. Rasanya enak dan gurih.','lanting_bumbu.jpg','2021-07-22 15:07:49','2021-07-22 15:07:49',NULL),(3,'Nopia','Makanan khas Banyumas nopia mempunyai bentuk yang dan rasa yang unik. Selain itu, cara membuatnya pun unik yaitu dengan cara ditempel di tungku khusus dengan suhu yang panas. Makany sering ada bagian dari nopia yang hangus. Bahan utamanya adalah tepung terigu yang diisi gula jawa atau kacang atau coklat.','nopia.jpg','2021-07-22 15:08:23','2021-07-22 15:08:24',NULL),(4,'Gethuk Goreng Sokaraja','Makanan khas Banyumas selanjutnya adalah getuk goreng sokaraja. Berbeda dari getuk pada umumnya karena pembuatannya memerlukan dua kali proses. Yang pertama yatu dengan mengukus bahan utamanya berupa singkong pilihan. Kemudian singkong yang sudah di kukus dilumatkan bersama gula jawa, campurkan juga dengan tepung terigu dan tepung beras. Bentuk menjadi kotak-kotak kecil. Proses selanjutnya adalah menggoreng hingga aromanya harum dan berubah warna. Getuk ini memiliki citarasa yang khas yaitu perpaduan manis gula jawa dan singkongnya.','gethuk_goreng_sokaraja.jpg','2021-07-22 15:09:18','2021-07-25 08:48:46',NULL);
/*!40000 ALTER TABLE `foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) unsigned NOT NULL,
  `notification_detail` text NOT NULL,
  `status` enum('Read','Unread') NOT NULL DEFAULT 'Unread',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_notifications_users` (`id_user`),
  CONSTRAINT `FK_notifications_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,2,'Booked','Unread','2021-07-19 21:24:15','2021-07-19 21:24:15',NULL);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `place_name` varchar(64) NOT NULL,
  `place_detail` text NOT NULL,
  `image` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (1,'Kebun Bunga Matahari Rempoah','Kebun bunga yang berisikan warna-warna cerah bunga matahari dan marigold','1-1-Rempoah-By-instapurwokerto.jpg','2021-07-21 16:16:10','2021-07-21 16:16:10',NULL),(2,'Bukit Pandang Munggang','Bukit Pandang yang harus kamu kunjungi jika ingin melihat keindahan alam dan lanskap yang keren dari kota Banyumas ini.','2-1-Munggang-By-fhm_atm.jpg','2021-07-21 16:17:09','2021-07-21 16:17:09',NULL);
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roles` varchar(12) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'Admin','2021-07-12 21:31:20','2021-07-12 21:31:21',NULL),(2,'Tourist','2021-07-12 21:31:29','2021-07-12 21:31:30',NULL),(3,'Tour Guide','2021-07-19 22:08:44','2021-07-19 22:08:44',NULL);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user_roles` bigint(20) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(64) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_roles` (`id_user_roles`),
  CONSTRAINT `FK_users_user_roles` FOREIGN KEY (`id_user_roles`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@gmail.com','$2y$10$0a2EmHDCcgbLXjW.hhwde.8.JjJNFzce9bFoz6qyrmcwWd3g.DPBi',NULL,'2021-07-15 02:53:09','2021-07-15 02:53:09',NULL),(2,2,'Bambang','bambang@gmail.com','$2y$10$0a2EmHDCcgbLXjW.hhwde.8.JjJNFzce9bFoz6qyrmcwWd3g.DPBi','4b9d63b0ad1de1873dd9ebf55ab7d391.682x682x1.jpg','2021-07-15 05:04:30','2021-07-15 05:04:30',NULL),(3,3,'Udin','udin@gmail.com','$2y$10$0a2EmHDCcgbLXjW.hhwde.8.JjJNFzce9bFoz6qyrmcwWd3g.DPBi','team-1.png','2021-07-19 22:09:05','2021-07-25 04:42:05',NULL),(4,3,'Bian Suma','biansuma21@gmail.com','$2y$10$lOQd1JT8VskxgoY5A7yaZuKcYz4FxSb8roDqX1QOEF4a9oDgAoCgK','20200825_115647.jpg','2021-07-25 05:30:14','2021-07-25 05:58:03',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-26  1:10:00
