jQuery(function($) {
    if ($('.sidebar-dropdown').hasClass("active")) {

    } else {
        $(".sidebar-submenu").hide();
    }

    $(".sidebar-dropdown > a").click(function() {
        if ($(this).parent().hasClass("active")) {
            $(".sidebar-submenu").slideUp(200);
            $(".sidebar-dropdown").removeClass("active");
            $(this).parent().removeClass("active");
        } else {
            $(".sidebar-dropdown").addClass("active");
            $(this).next(".sidebar-submenu").slideDown(200);
            $('.sidebar-submenu').removeClass("active");
        }
    })
})

$('.btn-book-pk-1').on('click', function() {
    $('.form-tour-guide').hide()
    $('[name=bookingpackageselect]').val('1')
})

$('.btn-book-pk-2').on('click', function() {
    $('.form-tour-guide').show()
    $('[name=bookingpackageselect]').val('2')
})

$('[name=bookingpackageselect]').on('change', function() {
    if (this.value == '2') {
        $('.form-tour-guide').show()
    } else {
        $('.form-tour-guide').hide()
    }
})