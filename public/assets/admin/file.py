import subprocess


def cloneApps(gitRepo):
    subprocess.run(["sudo", "git", "clone", gitRepo], check=True)
    subprocess.run(["chown", "-R", "www-data:www-data",
                    "adhyaksa-mism"], check=True)


def installPackages():
    installApache("apache2", "apache2-utils",
                  "ssl-cert", "libapache2-mod-wsgi")
    installNginx("nginx", "nginx.conf")
    installMariaDB("mariadb-server")
    installPHP("software-properties-common", "php7.1",
               "php7.1-cli",  "php7.1-common",  "php7.1-json",  "php7.1-opcache", "php7.1-mysql", "php7.1-mbstring", "php7.1-mcrypt", "php7.1-zip", "php7.1-fpm")
    startServer('nginx', 'apache2')


def installPHP(packageName1, packageName2, packageName3, packageName4, packageName5, packageName6, packageName7, packageName8, packageName9, packageName10, packageName11):
    try:
        subprocess.run(["sudo", "apt", "install", "-y",
                        packageName1], check=True)
        subprocess.run(["sudo", "add-apt-repository", "-y",
                        "ppa:ondrej/php"], check=True)
        subprocess.run(["sudo", "apt-get",
                        "update"], check=True)
        subprocess.run(["sudo", "apt-get", "install",
                        "-y", packageName2, packageName3, packageName4, packageName5, packageName6, packageName7, packageName8, packageName9, packageName10, packageName11], check=True)
    except:
        print('error')


def installApache(packageName1, packageName2, packageName3, packageName4):
    try:
        subprocess.run(["sudo", "apt", "install", "-y", packageName1,
                        packageName2, packageName3, packageName4], check=True)
        subprocess.run(["sudo", "systemctl", "stop", packageName1], check=True)
        site_available_config = open(
            '/etc/apache2/sites-available/000-default.conf')
        content = site_available_config.readlines()
        site_available_config.close()

        site_available_config = open(
            '/etc/apache2/sites-available/000-default.conf', 'w')
        content[0] = '<VirtualHost *:8066>\n'
        new_site_available_config = "".join(content)
        site_available_config.write(new_site_available_config)
        site_available_config.close()

        apache_config = open('/etc/apache2/apache2.conf')
        content = apache_config.readlines()
        apache_config.close()

        apache_config = open(
            '/etc/apache2/apache2.conf', 'w')
        for i in range(len(content)):
            if (content[i] == '<Directory /var/www/>\n'):
                content[i] = '<Directory /adhyaksa-mism/be-srm>\n'
            elif (content[i] == '<Directory />\n'):
                content[i + 3] = 'Require all granted\n'

        new_apache_config = "".join(content)
        apache_config.write(new_apache_config)
        apache_config.close()

        apache_port_config = open('/etc/apache2/ports.conf')
        content = apache_port_config.readlines()
        apache_port_config.close()

        apache_port_config = open(
            '/etc/apache2/ports.conf', 'w')
        for i in range(len(content)):
            if (content[i] == 'Listen 80\n'):
                content[i] = 'Listen 8066\n'

        new_apache_port_config = "".join(content)
        apache_port_config.write(new_apache_port_config)
        apache_port_config.close()

    except:
        print('error')


def installMariaDB(packageName):
    try:
        subprocess.run(["sudo", "apt", "install", "-y",
                        packageName], check=True)
    except:
        print('error')


def installNginx(packageName, fileName):
    try:
        subprocess.run(["sudo", "apt", "install",
                        "-y", packageName], check=True)
        subprocess.run(["sudo", "systemctl", "stop", packageName], check=True)
        subprocess.run(["sudo", "cp", fileName,
                        "/etc/nginx/"], check=True)
    except:
        print('error')


def startServer(server1, server2):
    subprocess.run(["sudo", "systemctl", "start", server1], check=True)
    subprocess.run(["sudo", "systemctl", "start", server2], check=True)


def installApp():
    installPackages()
    cloneApps(
        "http://uthasuryo:Sakosoy123@103.157.46.59/gitlab/team22/adhyaksa-mism.git")


installApp()
